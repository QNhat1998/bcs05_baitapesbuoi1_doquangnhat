const colorList = ["pallet", "viridian", "pewter", "cerulean",
    "vermillion", "lavender", "celadon", "saffron", "fuschia", "cinnabar"]

const getElement = id => document.getElementById(id)

btnColor = () => {
    let strBtnColor = ''

    colorList.map(color => {
        color === 'pallet' ? strBtnColor += `<button id='${color}' class='color-button ${color} active' onclick='changeColorHouse("${color}")'></button>`
            : strBtnColor += `<button id='${color}' class='color-button ${color}' onclick='changeColorHouse("${color}")'></button>
    `
    })
    getElement('colorContainer').innerHTML = strBtnColor
}

changeColorHouse = (color) => {
    let house = getElement('house'),
        btns = document.getElementsByClassName('color-button')
    for (let btn of btns) {
        if (btn.classList.contains('active')) {
            btn.classList.remove('active')
            house.classList.remove(btn.getAttribute('id'))
            break
        }

    }
    document.getElementsByClassName(`color-button ${color}`)[0].classList.add('active')
    house.classList.add(color)
}


btnColor()